# Transformation Exercise - Bulk Allocate IP Addresses - NetBox

## Loop Data Outgoing Schema
```json
[
    {
      "ipStatus": "",
      "dnsName": "",
      "prefix": ""
    }
]
```

## New Value Outgoing Schema
```json
{
  "ipStatus": "",
  "dnsName": "",
  "prefix": ""
}
```
