# JSON Form Exercise - Create and Update Incident - ServiceNow

## Request Body
```json
{
  "options": {
    "start": 0,
    "limit": 200,
    "sort": [
      {
        "name": 1
      }
    ],
    "order": "ascending",
    "filter": {
      "name": "OS"
    }
  }
}
```
