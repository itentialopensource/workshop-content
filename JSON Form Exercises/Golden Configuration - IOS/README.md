# JSON Form Exercise - Golden Configuration - IOS

## Request Body
```json
{
  "options": {
    "start": 0,
    "limit": 200,
    "sort": [
      {
        "name": 1
      }
    ],
    "order": "ascending"
  }
}
```
