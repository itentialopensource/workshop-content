# JSON Form Exercise - Create and Update Change Request - ServiceNow

## Request Body
```json
{
  "sysparmQuery": {
    "sysparm_limit": 10,
    "sys_class_name": "cmdb_ci_ip_router",
    "sysparm_fields": "sys_id,name"
  }
}
```
