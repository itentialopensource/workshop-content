# Golden Configuration Jinja2 Template Exercise - JUNOS

## JSON Data
```json
{
    "hostname": "device-1",
    "interfaces": [
        {
            "name": "ge-0/0/1",
            "subInterface": "100",
            "ipAddress": "192.168.1.1",
            "cidr": "/30"
        },
        {
            "name": "ge-0/0/2",
            "subInterface": "100",
            "ipAddress": "192.168.2.1",
            "cidr": "/30",
            "vlan": 102
        }
    ]
}
```
## Template Text
```jinja
set system host-name {{hostname}}

{% for interface in interfaces %}
set interfaces {{interface.name}} vlan-tagging
{% if interface.vlan %}
set interfaces {{interface.name}} unit {{interface.subInterface}} vlan-id {{interface.vlan}}
{% endif %}
set interfaces {{interface.name}} unit {{interface.subInterface}} family inet address {{interface.ipAddress}}{{interface.cidr}}

{% endfor %}
```
