# Jinja2 Template Exercise

## JSON Data
```json
{
  "firstName": "",
  "title": "",
  "companyName": "",
  "repeatableTask": "",
  "vendorNames": []
}
```
## Template Text
```jinja
My name is {{ firstName }}, and I'm a(n) {{ title }}.
Here at {{ companyName }}, we would like to automate {{ repeatableTask }}.
{% if vendorNames | length == 1 %}
The most common networking vendor I work with is {{ vendorNames[0] | title }}.
{% elif vendorNames | length > 1 %}
The most common networking vendors I work with are:
{% for vendor in vendorNames %}
 - {{ vendor | title }}
{% endfor %}
{% endif %}
```
