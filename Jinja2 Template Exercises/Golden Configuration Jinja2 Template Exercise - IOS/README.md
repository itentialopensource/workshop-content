# Golden Configuration Jinja2 Template Exercise - IOS

## JSON Data
```json
{
    "hostname": "device-1",
    "interfaces": [
        {
            "name": "gigabitEthernet 1",
            "ipAddress": "192.168.1.1",
            "subnetMask": "255.255.255.0"
        },
        {
            "name": "gigabitEthernet 2",
            "ipAddress": "192.168.2.1",
            "subnetMask": "255.255.255.0",
            "vlan": 102
        }
    ]
}

```
## Template Text
```jinja
conf t
hostname {{ hostname }}
{% for interface in interfaces %}
interface {{ interface.name }}
ip address {{ interface.ipAddress }} {{ interface.subnetMask }}
{% if interface.vlan %}
encapsulation dot1Q {{ interface.vlan }}
{% endif %}
no shutdown
exit
{% endfor %}
```
