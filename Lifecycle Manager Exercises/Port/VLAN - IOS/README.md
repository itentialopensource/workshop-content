# LCM Port/VLAN - IOS

## LCM Resource Model Properties
```json
{
  "autoApprove": {
    "type": "boolean"
  },
  "device": {
    "type": "string"
  },
  "type": {
    "type": "string"
  },
  "interface": {
    "type": "number"
  },
  "subInterface": {
    "type": "number"
  },
  "description": {
    "type": "string"
  },
  "ipAddress": {
    "type": "string"
  },
  "vlan": {
    "type": "number"
  },
  "subnetMask": {
    "type": "string"
  },
  "vlanNetboxID": {
    "type": "number"
  },
  "ipNetboxID": {
    "type": "number"
  }
}
```

## LCM Instance
```json
{
  "_id": "",
  "autoApprove": true,
  "device": "IOS",
  "type": "GigabitEthernet",
  "interface": 1,
  "description": "my interface"
}
```
