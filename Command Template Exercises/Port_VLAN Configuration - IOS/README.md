# Command Template Exercise - Port/VLAN Configuration - IOS

## First Example
### Command
```
show interface GigabitEthernet1
```
### Rule
```
GigabitEthernet1 is up
```
## Second Example
### Command
```
show interface <!interface!>
```
### Rule
```
<!interface!> is up
```
