# Command Template Exercise - Port/VLAN Configuration - EOS

## First Example
### Command
```
show interface Ethernet1
```
### Rule
```
Ethernet1 is up
```
## Second Example
### Command
```
show interface <!interface!>
```
### Rule
```
<!interface!> is up
```
