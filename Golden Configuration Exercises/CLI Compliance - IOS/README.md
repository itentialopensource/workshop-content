# CLI Compliance - IOS

## Base Node Configuration
```jinja
service password-encryption
<e/>hostname {{hostname}}
version {/17.[2-8]/}
ip ssh rsa keypair-name ssh-key
ip ssh version 2
line vty 0 4
 transport input ssh
{d/}vlan 1031
   {d/}name vlan1031
{% for i in interfaces %}
interface GigabitEthernet{{i.id}}
 ip address dhcp
 ip nat outside
 {d/}negotiation auto
 {d/}no mop enabled
 no mop sysid
{% endfor %}
```

## East Node Configuration
```jinja
snmp-server community east01 RO
```

## Variables
```json
{
  "hostname": "test",
  "interfaces": [
    {
      "id": "1"
    },
    {
      "id": "2"
    }
  ]
}
```
