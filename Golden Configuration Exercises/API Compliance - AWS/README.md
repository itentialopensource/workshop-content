# API Compliance - AWS

## Base Node Configuration
```json
{
  "response": {
    "DescribeSecurityGroupsResponse": {
      "securityGroupInfo": {
        "item": [
          {
            "ipPermissions": {
              "item": [
                {
                  "ipRanges": {
                    "item": [
                      {
                        "cidrIp": "192.168.84.0/24"
                      }
                    ]
                  }
                }
              ]
            }
          }
        ]
      }
    }
  }
}
```