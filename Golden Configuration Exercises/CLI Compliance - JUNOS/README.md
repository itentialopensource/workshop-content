# CLI Compliance - JUNOS

## Base Node Configuration
```jinja
version {/22.(4R1.10|4R2.8)/};
groups {
    aws-default {
        system {
            root-authentication {
                encrypted-password *disabled*;
            }
            scripts {
                translation {
                    max-datasize 512m;
                }
            }
            services {
                ssh;
                netconf {
                    ssh;
                }
                {d/}web-management {
                    {d/}https {
                        {d/}system-generated-certificate;
                    {d/}}
                {d/}}
            }
            license {
                autoupdate {
                    url https://ae1.juniper.net/junos/key_retrieval;
                }
            }
        }
        interfaces {
            fxp0 {
                unit 0 {
                    family inet {
                        dhcp;
                    }
                }
            }
        }
    }
}
system {
    <e/>host-name {{hostname}};
    services {
        ssh {
            root-login allow;
            sftp-server;
        }
        netconf {
            ssh;
        }
    }
}
interfaces {
    {% for i in interfaces %}
    ge-0/0/{{i.id}} {
        vlan-tagging;
    }
    {% endfor %}
}
```

## East Node Configuration
```jinja
apply-groups aws-default;
```

## Variables
```json
{
  "hostname": "test",
  "interfaces": [
    {
      "id": "1"
    },
    {
      "id": "2"
    }
  ]
}
```
